
package service;

import org.testng.Assert;
import org.testng.annotations.Test;
import static utils.Methods.*;
import main.Main;
import pageObjects.LoginPage_Component;

import pageObjects.MainPage_Component;

import pageObjects.OperationsPage_Component;


public class Service extends Main {
	
		
	@Test
	public void serviceTest() throws InterruptedException{
		
		
		/* Inicjacja PageObjects */
		
		LoginPage_Component loginPage = new LoginPage_Component(driver);		
		MainPage_Component mainPage = new MainPage_Component(driver);
		OperationsPage_Component operationsPage = new OperationsPage_Component(driver);
	   
		/* Koniec inicjacji PageObjects */
		
		if (parameters.hasParameter("result")) {
			
			if (parameters.getParameter("result").equals("fail")) {
							
			   Assert.assertTrue(false,"Wywolany blad");
			   
			}
			
		}
	
		report.logInfo("INFO WPIS");	
	    
		Thread.sleep(15000);
		parameters.setParameter("test_wy","wartosc");		
		
	}
	
	
	
}
